'''
Lab Number: 0
Assignment Name: Python Math Operators
Application Name: lab0.py

Assignment Description:
- In Exercises 1 through 12, evaluate the numeric expression without the computer
- and then use Python to check your answer.
'''

print("Programmer name: Klem N. Tine (use your own name)")
print("My second Python program")
print("Lab 1 Part B: Using the Python math operators")
print()
print("Python math operators")
print()
print(3*4)
print(7**2)
print(1/(2**3))
print(3+(4*5))
print((5-3)*4)
print(3*((-2)**5))
print(7//3)
print(14%4)
print(7%3)
print(14//4)
print(5//5)
print(5%5)
